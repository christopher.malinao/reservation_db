<?php
session_start();

// Check if the 'username' session variable is not set
if (!isset($_SESSION['username'])) {
   // User is not logged in, redirect to the login page
   header("Location: login.php");
   exit(); // Ensure that the script stops executing after redirection
}

include 'db.php';


// $id = $_GET['id'];
// // Delete the reservation from the database
// $sql = "DELETE FROM reservations WHERE id=$id";

// Delete the reservation from the database
// Sir Matthew's approach:
// $sql = "DELETE FROM reservations WHERE id = $_GET[id]";

$sql = "DELETE FROM reservations WHERE id=".$_GET['id'];

if($conn->query($sql) === TRUE){
	header("Location: index.php");
}
else{
	echo "Error deleting a reservation : " . $conn->error;
}

$conn->close();
?>