<?php
	session_start();

	if(!isset($_SESSION['username'])){
		header("Location: login.php");
		exit(); // Ensures that the script stops executing after redirection
	}

	include 'db.php';

	echo "Navbar || " . $_SESSION['username'] . " | <a href='logout.php'> Logout </a>" ;
	echo "<br><hr>";
	


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reservation System</title>
</head>
<body>
	<h1>Reservation System</h1>

	<form action="reserve.php" method="POST">

		<!-- Room Type Dropdown -->
		<label for="room_type">Room Type:</label>
        <select id="room_type" name="room_type" required>
            <option value="Single">Single</option>
            <option value="Double">Double</option>
            <option value="Suite">Suite</option>
        </select>

        <!-- User Name Input -->
        <label for="user_name">Your Name:</label>
        <input type="text" id="user_name" name="user_name" required>

        <!-- Reservation Date Input -->
        <label for="reservation_date">Reservation Date:</label>
        <input type="date" id="reservation_date" name="reservation_date" required>

        <!-- Discount Dropdown -->
        <label for="discount">Discount</label>
        <select id="discount" name="discount">
        	<?php

            	# Retrieve all the records in discounts table to add as options
        		$sql = "SELECT * FROM discounts";
        		$result = $conn->query($sql);

        		# A loop that will provide discounts as drop down options
        		if($result->num_rows > 0){
        			while($row = $result->fetch_assoc()){
        				echo 
        				"<option value='" . $row["id"] . "'>" . 
        				    $row["discount_name"] . " (-$" . $row["discount_amount"] . ")" . 
        				"</option>";
        			}
        		}

        	?>
        </select>
        <button type="submit">Reserve Room</button>

	</form>


	<!-- Display Reservations -->
    <h2>Reservations</h2>
    <!-- Search Form -->
    <form action="read.php" method="GET">
        <label for="search">Search by Room Type, User Name, or Discount:</label>
        <input type="text" id="search" name="search" placeholder="Enter search term">
        
        <label for="date">Search by Reservation Date:</label>
        <input type="date" id="date" name="date">
        
        <button type="submit">Search</button>
    </form>

    <?php
    	include 'read.php';
    ?>




</body>
</html>