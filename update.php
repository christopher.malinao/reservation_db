<?php
session_start();

if(!isset($_SESSION['username'])){
	header("Location: login.php");
	exit(); // Ensures that the script stops executing after redirection
}

echo "Navbar || " . $_SESSION['username'] . " | <a href='logout.php'> Logout </a>" ;
echo "<br><hr>";



include 'db.php';

if(isset($_GET['id'])){
	$id = $_GET['id'];
	// retrieves data that the application will update later on.
	$sql = "SELECT * FROM reservations WHERE id=$id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        // Display edit form with pre-filled data
        echo "<h2>Edit Reservation</h2>" .
         "<form action='update.php' method='POST'>" .
         "<input type='hidden' name='id' value='" . $row['id'] . "'>" .
         "Room Type: <input type='text' name='room_type' value='" . $row['room_type'] . "' required><br>" .
         "User Name: <input type='text' name='user_name' value='" . $row['user_name'] . "' required><br>" .
         "Reservation Date: <input type='date' name='reservation_date' value='" . $row['reservation_date'] . "' required><br>" .
         "<!-- Include dropdown for discounts, similar to the reservation form -->" .
         "<button type='submit'>Update Reservation</button>" .
         "</form>";
    } else {
        echo "Reservation not found.";
    }
}
else if($_SERVER['REQUEST_METHOD'] === POST){
	$id = $_POST['id'];
    $room_type = $_POST['room_type'];
    $user_name = $_POST['user_name'];
    $reservation_date = $_POST['reservation_date'];

    $sql = "UPDATE reservations SET room_type='$room_type', user_name='$user_name', reservation_date='$reservation_date' WHERE id=$id";

    if($conn->query($sql) === TRUE){
    	header("Location: index.php");
    }
    else{
    	echo "Error updating reservation : " . $conn->error;
    }
}
else{
	echo "Invalid request";
}

$conn->close();

?>